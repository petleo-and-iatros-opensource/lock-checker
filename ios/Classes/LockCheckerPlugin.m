#import "LockCheckerPlugin.h"
#if __has_include(<lock_checker/lock_checker-Swift.h>)
#import <lock_checker/lock_checker-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "lock_checker-Swift.h"
#endif

@implementation LockCheckerPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftLockCheckerPlugin registerWithRegistrar:registrar];
}
@end

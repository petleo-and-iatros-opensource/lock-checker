import Flutter
import UIKit
import LocalAuthentication

public class SwiftLockCheckerPlugin: NSObject, FlutterPlugin {
  public static func register(with registrar: FlutterPluginRegistrar) {
    let channel = FlutterMethodChannel(name: "lock_checker", binaryMessenger: registrar.messenger())
    let instance = SwiftLockCheckerPlugin()
    registrar.addMethodCallDelegate(instance, channel: channel)
  }

  public func handle(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
    result(LAContext().canEvaluatePolicy(.deviceOwnerAuthentication, error: nil))
  }
}

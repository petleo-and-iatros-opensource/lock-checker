import 'dart:async';

import 'package:flutter/services.dart';

class LockChecker {
  static const _channel = MethodChannel('lock_checker');

  static Future<bool?> isDeviceSecured() async {
    final isDeviceSecured = await _channel.invokeMethod('isDeviceSecured');
    return isDeviceSecured;
  }
}

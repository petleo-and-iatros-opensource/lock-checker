import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:lock_checker/lock_checker.dart';

void main() {
  const MethodChannel channel = MethodChannel('lock_checker');

  TestWidgetsFlutterBinding.ensureInitialized();

  setUp(() {
    channel.setMockMethodCallHandler((MethodCall methodCall) async {
      return true;
    });
  });

  tearDown(() {
    channel.setMockMethodCallHandler(null);
  });

  test('isDeviceSecured', () async {
    expect(await LockChecker.isDeviceSecured(), true);
  });
}
